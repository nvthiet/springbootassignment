# SpringBootAssignment

Asssignment For Spring Boot Training Course

Hướng dẫn chạy source code:

1. https://bitbucket.org/nvthiet/springbootassignment/ (public)
2. Tạo schema trong Mysql với tên "test"
3. Run project. (đã tạo một vài record để test)
4. Sử dụng postman để test. (có thể tải file "spring assignment.postman_collection.json" để import postman, đã có sẵn các api)

account admin:
	- username: amdin
	- password: 123456

account mod:
	- username: mod
	- password: 123456

5. Gọi API login để lấy token: 
		localhost:8080/api/login
		với json:
			{
				"username": "admin",
				"password": "123456"
			}
	respond sẽ trả về trong header với field Authorization, lấy token để gọi các API khác nếu cần permission admin

6. Các API theo yêu cầu:
	localhost:8080/api/user/username/1
	localhost:8080/api/user/startdate/1
	localhost:8080/api/user/membership/1

Vui lòng liên hệ Skype: thietnguyen25071995 nếu có gì thắc mắc.
	
	
