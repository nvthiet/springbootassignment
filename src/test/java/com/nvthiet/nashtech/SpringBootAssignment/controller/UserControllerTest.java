package com.nvthiet.nashtech.SpringBootAssignment.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.nvthiet.nashtech.SpringBootAssignment.DTO.UserMembership;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;
import com.nvthiet.nashtech.SpringBootAssignment.service.UserService;

public class UserControllerTest {

	private MockMvc mockMvc;

	@Mock
	UserService userService;

	@InjectMocks
	UserController userController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@Test
	public void findUsernameByIdWhenValidIdThenReturnUsername() throws Exception {

		User user = new User();
		user.setUsername("AAA");
		Optional<User> optionalUser = Optional.ofNullable(user);
		when(userService.findUserById(1)).thenReturn(optionalUser);

		this.mockMvc.perform(get("/api/user/username/1")).andExpect(status().isOk())
				.andExpect(jsonPath("$", is("AAA")));

		verify(userService, times(1)).findUserById(1);
	}

	@Test
	public void findUsernameByIdWhenInvalidIdThenReturnEmptyString() throws Exception {

		Optional<User> optionalUser = Optional.ofNullable(null);
		when(userService.findUserById(1)).thenReturn(optionalUser);

		this.mockMvc.perform(get("/api/user/username/1")).andExpect(status().isOk());

		verify(userService, times(1)).findUserById(1);
	}

	@Test
	public void findStartDateByIdWhenValidIdThenReturnDateString() throws Exception {

		String inputString = "14-06-2018";
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = dateFormat.parse(inputString);

		User user = new User();
		user.setUsername("AAA");
		user.setStartDate(date);
		Optional<User> optionalUser = Optional.ofNullable(user);
		when(userService.findUserById(1)).thenReturn(optionalUser);

		this.mockMvc.perform(get("/api/user/startdate/1")).andExpect(status().isOk());

		verify(userService, times(1)).findUserById(1);
	}

	@Test
	public void findStartDateByIdWhenInvalidIdThenReturnEmptyString() throws Exception {

		Optional<User> optionalUser = Optional.ofNullable(null);
		when(userService.findUserById(1)).thenReturn(optionalUser);

		this.mockMvc.perform(get("/api/user/startdate/1")).andExpect(status().isOk());

		verify(userService, times(1)).findUserById(1);
	}

	@Test
	public void findUserMembershipByIdWhenValidIdThenReturnMemberShip() throws Exception {
		
		UserMembership userMembership = new UserMembership();
		userMembership.setId(1);
		userMembership.setName("AAA");
		userMembership.setLoyal_point(10);

		when(userService.findUserMemberShip(1)).thenReturn(userMembership);

		this.mockMvc.perform(get("/api/user/membership/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.name", is("AAA")))
				.andExpect(jsonPath("$.loyal_point", is(10)))
				;

		verify(userService, times(1)).findUserMemberShip(1);
	}
}
