package com.nvthiet.nashtech.SpringBootAssignment.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nvthiet.nashtech.SpringBootAssignment.DTO.UserMembership;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;
import com.nvthiet.nashtech.SpringBootAssignment.repository.UserRepository;
import com.nvthiet.nashtech.SpringBootAssignment.service.implement.UserServiceImplement;

public class UserServiceImplementTest {

	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserServiceImplement userServiceImplement;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	// test method findUseById()
	@Test
	public void findUserByIdWhenValidIdThenReturnUser() {
		
		User user = new User();
		Optional<User> optionalUser = Optional.ofNullable(user);
		when(userRepository.findById(1)).thenReturn(optionalUser);
		
		assertEquals(Optional.of(user), userServiceImplement.findUserById(1));
		
		verify(userRepository, times(1)).findById(1);
	}
	
	@Test
	public void findUserByIdWhenInvalidIdThenReturnNull() {
		
		Optional<User> optionalUser = Optional.ofNullable(null);
		when(userRepository.findById(3)).thenReturn(optionalUser);
		
		assertEquals(Optional.empty(), userServiceImplement.findUserById(1));
		
		verify(userRepository, times(1)).findById(1);
	}
	
	// test method findUserMemberShip()
	@Test
	public void findUserMemberShipWhenValidIdThenReturnUserMembership() throws ParseException {
		
		String inputString = "14-06-2018";
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = dateFormat.parse(inputString);
		
		User user = new User();
		user.setId(1);
		user.setStartDate(date);
		user.setUsername("aaa");
		
		Optional<User> optionalUser = Optional.of(user);
		
		when(userRepository.findById(1)).thenReturn(optionalUser);
		
		UserMembership userMembership = userServiceImplement.findUserMemberShip(1);
		System.out.println(userMembership.getLoyal_point());
		
		assertTrue( userMembership.getId().equals(1) && userMembership.getName().equals("aaa")
				&& userMembership.getStart_date().equals(date)
				&& userMembership.getLoyal_point() == 10);
		
		verify(userRepository, times(1)).findById(1);
	}
	
	@Test
	public void findUserMemberShipWhenInValidIdThenReturnNull() throws ParseException {
		
		Optional<User> optionalUser = Optional.ofNullable(null);
		
		when(userRepository.findById(1)).thenReturn(optionalUser);
		
		UserMembership userMembership = userServiceImplement.findUserMemberShip(1);
		
		assertEquals(userMembership, null);
		
		verify(userRepository, times(1)).findById(1);
	}
}
