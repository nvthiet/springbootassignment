package com.nvthiet.nashtech.SpringBootAssignment.service.implement;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nvthiet.nashtech.SpringBootAssignment.DTO.UserMembership;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;
import com.nvthiet.nashtech.SpringBootAssignment.repository.UserRepository;
import com.nvthiet.nashtech.SpringBootAssignment.service.UserService;

@Service
public class UserServiceImplement implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	public Optional<User> findUserById(int id) {
		return userRepository.findById(id);
	}

	public UserMembership findUserMemberShip(int id) {
		
		Optional<User> user= userRepository.findById(id);
		
		if (user.isPresent()) {
			
			UserMembership userMembership = new UserMembership();
			userMembership.setId(user.get().getId());
			userMembership.setName(user.get().getUsername());
			userMembership.setStart_date(user.get().getStartDate());
			
			long daydiff = getDateDiff(user.get().getStartDate(), new Date(), TimeUnit.DAYS);
			
			userMembership.setLoyal_point((daydiff + 1)*5);
			
			return userMembership;
		}
		return null;
	}
	
	private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
}
