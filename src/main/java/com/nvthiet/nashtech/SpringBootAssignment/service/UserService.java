package com.nvthiet.nashtech.SpringBootAssignment.service;

import java.util.Optional;

import com.nvthiet.nashtech.SpringBootAssignment.DTO.UserMembership;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;


public interface UserService {

	public Optional<User> findUserById(int id);
	public UserMembership findUserMemberShip(int id);
}
