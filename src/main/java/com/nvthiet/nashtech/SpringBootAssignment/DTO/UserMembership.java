package com.nvthiet.nashtech.SpringBootAssignment.DTO;

import java.util.Date;

public class UserMembership {

	private Integer id;

	private String name;

	private Date start_date;

	private long loyal_point;
	
	public UserMembership() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public long getLoyal_point() {
		return loyal_point;
	}

	public void setLoyal_point(long loyal_point) {
		this.loyal_point = loyal_point;
	}

	@Override
	public String toString() {
		return "UserMembership [id=" + id + ", name=" + name + ", start_date=" + start_date + ", loyal_point="
				+ loyal_point + "]";
	}

}
