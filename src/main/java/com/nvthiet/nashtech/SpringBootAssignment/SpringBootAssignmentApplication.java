package com.nvthiet.nashtech.SpringBootAssignment;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.nvthiet.nashtech.SpringBootAssignment.entities.Account;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;
import com.nvthiet.nashtech.SpringBootAssignment.repository.AccountRepository;
import com.nvthiet.nashtech.SpringBootAssignment.repository.UserRepository;

@SpringBootApplication
public class SpringBootAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAssignmentApplication.class, args);
	}
	
	@Bean
    public CommandLineRunner demoData(UserRepository userRepo, AccountRepository accRepo,PasswordEncoder passwordEncoder) {
        return args -> { 
        	
        	// initialize users
        	User user1 = new User();
        	user1.setUsername("aaaaaa");
        	user1.setStartDate(new Date());
        	
        	User user2 = new User();
        	user2.setUsername("bbbbbb");
        	
        	LocalDate localDate = LocalDate.of(2018, 06, 01);
        	user2.setStartDate(new Date().from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        	
        	userRepo.save(user1);
        	userRepo.save(user2);
        	
        	// initialize account for spring security
        	
        	Account acc1 = new Account();
        	acc1.setUsername("admin");
        	acc1.setPassword(passwordEncoder.encode("123456"));
        	acc1.setRole("ADMIN");
        	accRepo.save(acc1);
        	
        	Account acc2 = new Account();
        	acc2.setUsername("mod");
        	acc2.setPassword(passwordEncoder.encode("123456"));
        	acc2.setRole("MOD");
        	accRepo.save(acc2);
        };
    }
}
