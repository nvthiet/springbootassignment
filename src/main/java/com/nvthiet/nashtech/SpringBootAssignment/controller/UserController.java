package com.nvthiet.nashtech.SpringBootAssignment.controller;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nvthiet.nashtech.SpringBootAssignment.DTO.UserMembership;
import com.nvthiet.nashtech.SpringBootAssignment.entities.User;
import com.nvthiet.nashtech.SpringBootAssignment.service.UserService;

@RestController
@RequestMapping(value = UserController.URI.API)
public class UserController {

	public static final class URI {
		
		public static final String API = "/api/user";
		
		public static final String USER_USERNAME = "/username/{id}";
		
		public static final String USER_STARTDATE = "/startdate/{id}";
		
		public static final String USER_MEMBERSHIP = "/membership/{id}";
		
	}
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET, value = URI.USER_USERNAME)
	public String findUsernameById(@PathVariable("id") int id) {
		
		Optional<User> user = userService.findUserById(id);
		
		if(user.isPresent()) {
			return user.get().getUsername();
		}
		return "";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = URI.USER_STARTDATE)
	public Date findStartDateById(@PathVariable("id") int id) {
		
		Optional<User> user = userService.findUserById(id);
		
		if(user.isPresent()) {
			return user.get().getStartDate();
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = URI.USER_MEMBERSHIP)
	public UserMembership findUserMembershipById(@PathVariable("id") int id) {
		
		UserMembership userMembership = userService.findUserMemberShip(id);
		return userMembership;
	}
	
}
