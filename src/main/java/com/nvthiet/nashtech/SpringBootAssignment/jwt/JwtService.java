package com.nvthiet.nashtech.SpringBootAssignment.jwt;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtService {

	public String genarateJwtToken(Authentication authResult) {
		Collection<? extends GrantedAuthority> authorities = authResult.getAuthorities();
		GrantedAuthority authority = authorities.iterator().next();
		String authorityString = authority.getAuthority();
		
		String token = Jwts.builder()
				.setSubject(authResult.getName())
				.claim("authority", authorityString)
				.signWith(SignatureAlgorithm.HS256, "mySecret".getBytes())
				.compact();
		return token;
	}

	public Authentication parseJwtToken(String token) {
		Claims claims = Jwts.parser()
				.setSigningKey("mySecret".getBytes())
				.parseClaimsJws(token)
				.getBody();	

		String username = claims.getSubject();
		String authorityString = (String) claims.get("authority");
		
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority(authorityString);
		Collection<? extends GrantedAuthority> authorities = Collections.singletonList(authority);

		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				username, null, authorities);
		
		return auth;
	}
}