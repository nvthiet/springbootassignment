package com.nvthiet.nashtech.SpringBootAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nvthiet.nashtech.SpringBootAssignment.entities.Account;

public interface AccountRepository extends JpaRepository<Account, Integer>{

	Account findByUsername(String username);
}
