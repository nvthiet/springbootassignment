package com.nvthiet.nashtech.SpringBootAssignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nvthiet.nashtech.SpringBootAssignment.entities.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
