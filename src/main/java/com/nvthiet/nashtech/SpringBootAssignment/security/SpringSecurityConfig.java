package com.nvthiet.nashtech.SpringBootAssignment.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.nvthiet.nashtech.SpringBootAssignment.jwt.JwtAuthenticationFilter;
import com.nvthiet.nashtech.SpringBootAssignment.jwt.JwtVerifyFilter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf()
			.disable()
			.cors()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.antMatcher("/api/**")
			.authorizeRequests()
				.antMatchers("/api/user/username/**").permitAll()
				.antMatchers("/api/user/startdate/**").hasAnyRole("ADMIN", "MOD")
				.antMatchers("/api/user/membership/**").hasRole("ADMIN")
			.and()
			.addFilterAfter(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
			.addFilterAfter(jwtVerifyFilter(), UsernamePasswordAuthenticationFilter.class)
			;
	}
	
	@Bean
	JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
		JwtAuthenticationFilter filter = new JwtAuthenticationFilter("/api/login");
		filter.setAuthenticationManager(authenticationManager());
		return filter;
	}
	
	@Bean
	JwtVerifyFilter jwtVerifyFilter() throws Exception {
		return new JwtVerifyFilter();
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
