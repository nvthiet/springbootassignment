package com.nvthiet.nashtech.SpringBootAssignment.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.nvthiet.nashtech.SpringBootAssignment.entities.Account;
import com.nvthiet.nashtech.SpringBootAssignment.repository.AccountRepository;

@Component
public class DatabaseUserDetailsService implements UserDetailsService {

	@Autowired
	private AccountRepository accRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Account acc = accRepository.findByUsername(username);
		
		if (acc == null) {
			throw new UsernameNotFoundException(username);
		}
		
		UserPrincipal userPrincipal = new UserPrincipal(acc.getUsername(), acc.getPassword(), acc.getRole());
		
		return userPrincipal;
	}

}