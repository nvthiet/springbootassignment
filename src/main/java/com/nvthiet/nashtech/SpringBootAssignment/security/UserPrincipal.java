package com.nvthiet.nashtech.SpringBootAssignment.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class UserPrincipal extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;

	public UserPrincipal(String username, String password, String roles) {
		super(username, password, convertRoleToAuthorities(roles));
	}

	private static Collection<? extends GrantedAuthority> convertRoleToAuthorities(String roles) {
		Collection<GrantedAuthority> authorities = new ArrayList<>();

		authorities.add(new SimpleGrantedAuthority("ROLE_" + roles));

		return authorities;
	}

}